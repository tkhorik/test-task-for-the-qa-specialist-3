import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {

        System.out.println(myFactorialMethod(20));

        Scanner fnumber = new Scanner(System.in);
        int number = Integer.parseInt(fnumber.nextLine());
        System.out.println(myFactorialMethod(number));
    }

    private static Long myFactorialMethod(int n) {
        if (n == 0) {
            return 1L;
        }
        Long fact = 1L;
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }
}